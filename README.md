---
page_type: sample
description: "This is a minimal sample app that demonstrates how to run a Python Flask application on Azure App Service on Linux."
languages:
- python
products:
- azure
- azure-app-service
---

# Python Flask sample for Azure App Service (Linux)

This is a minimal sample app that demonstrates how to run a Python Flask application on Azure App Service on Linux.

For more information, please see the [Python on App Service quickstart](https://docs.microsoft.com/azure/app-service/containers/quickstart-python).

## Contributing

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.


az webapp up -n onedpo-data-mapping --resource-group solutionTemplate2 -p solutionTemplateAppServicePlan --sku F1 --location westus2 --output json

az webapp config appsettings list -n onedpo-data-mapping --resource-group solutionTemplate2

az webapp config appsettings set -n onedpo-data-mapping --resource-group solutionTemplate2 --settings MONGO_URI="mongodb://mongodb-ucmdrnabandn4:7HMTgdJTfnPf0VBBw38jbiq9i3DcZ7kQlXwdXNehHJ4BnLCrC0a3YZrgE2ruSA8Dg89EqiHUnZ8To84g85XSQA==@mongodb-ucmdrnabandn4.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&maxIdleTimeMS=120000&appName=@mongodb-ucmdrnabandn4@"

az webapp restart -n onedpo-data-mapping --resource-group solutionTemplate2

az webapp create --name onedpo-data-mapping --resource-group solutionTemplate2 --plan solutionTemplateAppServicePlan --deployment-source-branch master --deployment-source-url https://naresh_onedpo@bitbucket.org/naresh_onedpo/azure-python-app-service.git --runtime "python|3.7" --output json

az webapp list-runtimes


### To Run script

```bash
sudo docker build -t onedpo/python-rest-app .

sudo bash azure-vm-deploy.sh \
    -m 'mongodb://onedpo-mongo:27017/test' \
    -d 'test' \
    -b 'redis://onedpo-redis:6379/0' \
    -r 'redis://onedpo-redis:6379/0'

```

