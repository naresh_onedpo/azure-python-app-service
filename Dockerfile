FROM python:3.7.2

ARG PORT=1234
ENV PORT=${PORT}

ENV ONEDPO_MONGO_DBNAME=test
ENV ONEDPO_MONGO_URI=mongodb://localhost:27017/test

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN pip install -r requirements.txt
# RUN python3 -m spacy download en

EXPOSE ${PORT}
ENTRYPOINT ["sh", "start.sh"]