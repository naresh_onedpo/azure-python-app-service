"""App entry point."""
from application import app
# from dotenv import load_dotenv, find_dotenv
# load_dotenv(find_dotenv())


if __name__ == "__main__":
    app.run(host='0.0.0.0', port="1234" , debug=True)