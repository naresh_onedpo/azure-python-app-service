# Read script parameters
while getopts m:d:b:r: option
do
case "${option}"
in
m) ONEDPO_MONGO_URI=${OPTARG};;
d) ONEDPO_MONGO_DBNAME=${OPTARG};;
b) ONEDPO_CELERY_BROKER_URL=${OPTARG};;
r) ONEDPO_CELERY_RESULT_BACKEND=${OPTARG};;
esac
done

# dependent docker services
PRESIDIO_URL=http://onedpo-presidio-api:8080

sudo docker stop onedpo-python-rest;

sudo docker rmi onedpo/python-rest-app

sudo docker pull onedpo/python-rest-app

sudo docker run --rm -d --name onedpo-python-rest \
--network shared \
-p 1234:1234 \
-e ONEDPO_MONGO_URI=$ONEDPO_MONGO_URI \
-e ONEDPO_MONGO_DBNAME=$ONEDPO_MONGO_DBNAME \
-e ONEDPO_CELERY_BROKER_URL=$ONEDPO_CELERY_BROKER_URL \
-e ONEDPO_CELERY_RESULT_BACKEND=$ONEDPO_CELERY_RESULT_BACKEND \
onedpo/python-rest-app