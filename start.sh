#!/bin/sh
# https://stackoverflow.com/questions/43925487/how-to-run-gunicorn-on-docker
exec gunicorn -b :$PORT -t 180 --log-level=debug wsgi:app