import os
import json
from pymongo import MongoClient
from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"


@app.route("/env")
def cosmos():
    data = {}
    # mongo_url = os.getenv('ONEDPO_MONGO_URI', "")
    # if mongo_url:
    #     # uri = "mongodb://mongodb-ucmdrnabandn4:7HMTgdJTfnPf0VBBw38jbiq9i3DcZ7kQlXwdXNehHJ4BnLCrC0a3YZrgE2ruSA8Dg89EqiHUnZ8To84g85XSQA==@mongodb-ucmdrnabandn4.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&maxIdleTimeMS=120000&appName=@mongodb-ucmdrnabandn4@"
    #     client = MongoClient(mongo_url)
    #     data = dict((db, [collection for collection in client[db].collection_names()])
    #                 for db in client.database_names())
    # else:
    #     data = {'message': "No mongo url in env variable"}
    for param in os.environ.keys():
        if "ONEDPO" in param:
            print("%20s %s" % (param,os.environ[param]))
            data[param] = os.environ[param]
    try:
        mongo_url = os.getenv('ONEDPO_MONGO_URI', "mongo_url")
        db_name = os.getenv('ONEDPO_MONGO_DBNAME', "db_name")

        client = MongoClient(mongo_url)
        db = client[db_name]
        collections = db.list_collection_names()
        data['collections'] = collections
    except Exception as e:
        print(e)
    return json.dumps(data)